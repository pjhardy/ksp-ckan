{
  "spec_version": "1.16",
  "name": "Kerbal Simpit",
  "author": "stibbons",
  "abstract": "Communicate with external instrumentation through serial connection.",
  "identifier": "KerbalSimpit",
  "download": "https://bitbucket.org/pjhardy/kerbalsimpit/downloads/ZIPNAME",
  "license": "BSD-2-clause",
  "$vref": "#/ckan/ksp-avc",
  "release_status": "stable",
  "ksp_version_strict": true,
  "resources": {
    "repository": "https://bitbucket.org/pjhardy/kerbalsimpit",
    "ci": "https://home.hardy.dropbear.id.au/bamboo/browse/KSPIT-KSPIT"
  },
  "recommends": [
    { "name": "AlternateResourcePanel" }
  ],
  "suggests": [
    { "name": "AGExt" }
  ]
}
