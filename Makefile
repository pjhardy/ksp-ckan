# The Bamboo build exports the most recent build artefact
# to the current directory. Use it if it exists.
# Otherwise use the oldest published release.
ifneq ("$(wildcard *.zip)","")
	ZIPNAME := $(shell ls *.zip|tail -1)
else
	ZIPNAME := KerbalSimpit-12.zip
endif

all: KerbalSimpit.ckan

clean:
	rm -f *.netkan
	rm -f *.ckan

KerbalSimpit.ckan: KerbalSimpit.netkan
	mono /usr/local/bin/netkan.exe KerbalSimpit.netkan

KerbalSimpit.netkan:
	m4 -DZIPNAME=$(ZIPNAME) KerbalSimpit.netkan.m4 > KerbalSimpit.netkan
